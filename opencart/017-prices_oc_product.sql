ALTER TABLE admin_oc.oc_product ADD price_ml decimal(15,4) NULL;
ALTER TABLE admin_oc.oc_product ADD price_b2w Decimal(15,4) NULL;
ALTER TABLE admin_oc.oc_product ADD price_magalu decimal(15, 4) NULL;
ALTER TABLE admin_oc.oc_product ADD date_target_price DATETIME NULL;
