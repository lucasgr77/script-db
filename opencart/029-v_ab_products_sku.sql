CREATE OR REPLACE VIEW admin_oc.v_ab_products_sku
AS select ap.*, oc.sku from oc_product oc, products_association pa, ab_products ap
where oc.product_id  = pa.product_id 
and pa.codproduto  = ap.CODPRODUTO 