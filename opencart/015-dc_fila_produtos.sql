CREATE TABLE admin_oc.dc_fila_produtos (
	id INT auto_increment NOT NULL,
	sku varchar(200) NOT NULL,
	preco DECIMAL NULL,
	status INT NOT NULL,
	retry INT NOT NULL,
	data_inserido DATETIME NOT NULL,
	data_atualizado DATETIME NULL,
	CONSTRAINT dc_fila_produtos_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci;
