
CREATE TABLE admin_oc.sn_conference (
	product_id INT NULL,
	codproduto INT NULL,
	done VARCHAR(1) NULL,
	localizacao varchar(140) NULL,
	loja INT NOT NULL,
	usuario varchar(100) NULL
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci
COMMENT='Tabela de indicadores a serem conferidos pelo app do Sentinela';
